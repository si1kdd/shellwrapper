#include <glob.h>
#include "parse.h"
#include "exec.h"

char *read_line()
{
        int buff_size = MAX_CMDS;
        int pos = 0;

        char *line = (char *) malloc(buff_size * sizeof(char));
        if (!line) exit(-1);

        int c;
        while(1)
        {
                c = getchar();
                if (c == '\n') {
                        line[pos] = '\0';
                        return line;
                }
                else if (c == EOF)
                        run_exit();
                else
                        line[pos] = c;

                pos++;
                if (pos >= buff_size)
                {
                        buff_size *= 2;
                        line = (char *) realloc(line, buff_size);
                        if (!line) exit(-1);
                }
        }
}

int get_cmds_type(char *cmds)
{
        if (!strcmp(cmds, "exit"))
                return EXIT;
        else if (!strcmp(cmds, "cd"))
                return CD;
        else if (!strcmp(cmds, "fg"))
                return FG;
        else if (!strcmp(cmds, "bg"))
                return BG;
        else if (!strcmp(cmds, "export"))
                return EXPORT;
        else if (!strcmp(cmds, "unset"))
                return UNSET;
        else if (!strcmp(cmds, "jobs"))
                return JOBS;
        else
                return EXTERNAL;
}

Process *parse_tokens(char *slice)
{
        char *cmds = strdup(slice);
        // printf("%s\n", cmds);
        int buff_size = MAX_TOKENS;
        int pos = 0;

        char **tks = (char **) malloc(buff_size * sizeof(char*));
        if (!tks) exit(-1);

        char *tk = strtok(slice, " \t\r\n\a");
        while (tk != NULL)
        {
                int expand_count = 0;
                glob_t expand_str;
                if (strchr (tk, '*') != NULL || strchr(tk, '?') != NULL) {
                        glob(tk, 0, NULL, &expand_str);
                        expand_count = expand_str.gl_pathc;
                }

                if (pos + expand_count >= buff_size) {
                        buff_size *= 2;
                        buff_size += expand_count;
                        tks = (char **) realloc (tks, buff_size * sizeof(char *));
                        if (!tks) exit(-1);
                }
                if (expand_count > 0) {
                        for (int i = 0; i < expand_count; ++i)
                                tks[pos++] = strdup(expand_str.gl_pathv[i]);
                        globfree(&expand_str);
                } else {
                        tks[pos] = tk; pos++;
                }
                tk = strtok(NULL, " \t\r\n\a");
        }

        char *path_in = NULL;
        char *path_out = NULL;
        int index = 0;
        int argc = 0;
        for (; index < pos; index++)
        {
                if (tks[index][0] == '<' || tks[index][0] == '>')
                        break;
        }

        argc = index;   // remark.

        for (; index < pos; index++)
        {
                int len = strlen(tks[index]);
                if (tks[index][0] == '<') {
                        if (len == 1) {
                                int next_len = strlen(tks[index + 1]);
                                path_in = (char *) malloc((next_len + 1) * sizeof(char));
                                strcpy(path_in, tks[index + 1]); index++;
                        } else {
                                path_in = (char *) malloc(len * sizeof(char));
                                strcpy(path_in, tks[index] + 1);
                        }
                } else if (tks[index][0] == '>') {
                        if (len == 1) {
                                int next_len = strlen(tks[index + 1]);
                                path_out = (char *) malloc((next_len + 1) * sizeof(char));
                                strcpy(path_out, tks[index + 1]); index++;
                        } else {
                                path_out = (char *) malloc(len * sizeof(char));
                                strcpy(path_in, tks[index] + 1);
                        }
                } else break;
        }
        for (index = argc; index <= pos; index++)
                tks[index] = NULL;

        Process *new_p = (Process *) malloc(sizeof(Process));
        new_p->cmds = cmds;
        new_p->argv = tks;
        new_p->argc = argc;
        new_p->path_in = path_in; new_p->path_out = path_out;
        // check.
        if (path_in != NULL && path_out != NULL)
                printf("%s -> %s", path_in, path_out);
        new_p->pid = -1;
        new_p->type = get_cmds_type(tks[0]);
        new_p->next_proc = NULL;
        return new_p;
}

ProcessGroup *parse_cmds(char *line)
{
        int mode = FRONT;
        // remove space.
        char *h = line;
        char *e = line + strlen(line);
        for (; *h == ' '; h++);
        for (; *e == ' '; e--);
        *(e + 1) = '\0';
        line = h;

        // remark.
        char *cmds = strdup(line);
        // printf("%ld\n", strlen(line));

        // bg command.
        int len = strlen(line);
        if (line[len - 1] == '&') {
                mode = BACK;
                line[len - 1] = '\0';
        }

        Process *root_p = NULL;
        Process *p = NULL;
        char *slice_ptr = line;
        char *ptr = line;
        char *slice;
        int slice_len = 0;

        while (1)
        {
                if (*ptr == '\0' || *ptr == '|') {
                        slice = (char *) malloc((slice_len + 1) * sizeof(char));
                        strncpy(slice, slice_ptr, slice_len);
                        slice[slice_len] = '\0';
                        Process *new_p = parse_tokens(slice);
                        // printf("%d\n", new_p->argc);
                        if (!root_p) {
                                root_p = new_p; p = root_p;
                        } else {
                                p->next_proc = new_p;
                                p = new_p;
                        }
                        if (*ptr != '\0') {
                                slice_ptr = ptr;
                                do { ++slice_ptr; } while (*slice_ptr == ' ');
                                ptr = slice_ptr;
                                slice_len = 0;
                                continue;
                        } else
                                break;
                } else {
                        slice_len++; ptr++;
                }
        }

        ProcessGroup *new_group = (ProcessGroup *) malloc(sizeof(ProcessGroup));
        new_group->root_proc = root_p;
        new_group->commands = cmds;
        new_group->mode = mode;
        new_group->pgid = -1;
        // check.
        // printf("%d\n", new_group->root_proc->argc);
        return new_group;
}
