#define UNUSED(x) (void)(x)

#define _POSIX_C_SOURCE 200112L

#include <pwd.h>
#include "utils.h"
#include "parse.h"
#include "print.h"
#include "signal.h"
#include "exec.h"

static void __init()
{
        struct sigaction sigint_action;
        sigint_action.sa_handler = &sigint_handler;
        sigint_action.sa_flags = 0;

        sigemptyset(&sigint_action.sa_mask);
        sigaction(SIGINT, &sigint_action, NULL);
        signal(SIGQUIT, SIG_IGN); signal(SIGTSTP, SIG_IGN);
        signal(SIGTTIN, SIG_IGN);

        pid_t pid = getpid();
        setpgid(pid, pid);
        tcsetpgrp(0, pid);
        curr_shell = (Shell*) malloc(sizeof(Shell));
        int size_of_curr_user = sizeof(curr_shell->curr_user);

        getlogin_r(curr_shell->curr_user, size_of_curr_user);

        struct passwd *pwd = getpwuid(getuid());
        strcpy(curr_shell->pwd_dir, pwd->pw_dir);

        for (int i = 0; i < NR_GROUPS; ++i)
                curr_shell->pgs[i] = NULL;

        getcwd(curr_shell->curr_dir, sizeof(curr_shell->curr_dir));
}

static void __loop()
{
        char *input;
        ProcessGroup *pg;
        // default done.
        int status = DONE;
        while (1)
        {
                __print_prompt();
                input = read_line();
                if (strlen(input) == 0) {
                        scan_zombie();
                        continue;
                }

                /* start the job */
                pg = parse_cmds(input);
                // printf("line: %s\n", pg->commands);
                status = exec_pgs(pg);
        }
        UNUSED(status);
}

int main(void)
{
        __init();
        __loop();
        return 0;
}
