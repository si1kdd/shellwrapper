#include "print.h"

void __print_prompt()
{
        printf("[shell-wrapper in \033[1;35mdir:%s\033[0m  \033[40m\033[1;32m$\033[0m ",
                        curr_shell->curr_dir);
}

int __print_group_status(int index)
{
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return -1;

        printf("[%d]", index);

        const char *status_string[] = { "running", "done", "suspended", "continued", "terminated" };

        for (Process *p = curr_shell->pgs[index]->root_proc;
                        p != NULL; p = p->next_proc)
        {
                printf("\t %d %s\t %s", p->pid,
                                status_string[p->status], p->cmds);
                if (p->next_proc != NULL)
                        printf("|\n");
                else
                        printf("\n");
        }

        return 0;
}

int __print_the_procs(int index)
{
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return -1;
        printf("[%d]", index);

        for (Process *p = curr_shell->pgs[index]->root_proc;
                        p != NULL; p = p->next_proc)
                printf(" %d", p->pid);
        printf("\n");
        return 0;
}
