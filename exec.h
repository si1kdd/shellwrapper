#ifndef __EXEC_H__
#define __EXEC_H__

#include "utils.h"
#include "groups.h"
#include "print.h"
#include "signal.h"

int run_cd(int argc, char **argv);
int run_fg(int argc, char **argv);
int run_bg(int argc, char **argv);
int run_jobs(int argc, char **argv);
int run_export(int argc, char **argv);
int run_unset(int argc, char **argv);
int run_exit();

int exec_pgs(ProcessGroup *pgs);
int exec_procs(ProcessGroup *pg, Process *p, int fd_in, int fd_out, int mode);
int exec_builtin_cmds(Process* p);

#endif
