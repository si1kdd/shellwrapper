TAREGET = shell_wrapper

UNAME := $(shell uname -s)
ifeq ($(UNAME), $(filter $(UNAME), Darwin FreeBSD))
	CC=clang
else
	CC=gcc
endif

CFLAGS 	= -std=gnu99 -Wall -Wextra -Wpedantic -O2 -Os -fno-stack-protector
OBJECTS = shell.o groups.o parse.o exec.o print.o signal.o

all: ${TAREGET}

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

$(TAREGET) : $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@
	strip -s $@

clean:
	rm -rf ${TAREGET} *.o
