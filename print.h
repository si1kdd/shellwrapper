#ifndef __PRINT_H__
#define __PRINT_H__

#include "utils.h"

void __print_prompt();
int __print_group_status(int);
int __print_the_procs(int index);

#endif
