#include "exec.h"
#define UNUSED(x) (void)(x)

int run_cd(int argc, char **argv)
{
        if (argc == 1) {
                chdir (curr_shell->pwd_dir);
                getcwd(curr_shell->curr_dir, sizeof(curr_shell->curr_dir));
                return 0;
        }
        if (chdir(argv[1]) == 0) {
                getcwd(curr_shell->curr_dir, sizeof(curr_shell->curr_dir));
                return 0;
        } else {
                printf("cd %s: No such file or directory\n", argv[1]);
                return 0;
        }
}

int run_jobs(int argc, char **argv)
{
        UNUSED(argc); UNUSED(argv);
        for (int i = 0; i < NR_GROUPS; i++)
        {
                // be careful of the shell env.
                if (curr_shell->pgs[i] != NULL)
                        __print_group_status(i);
        }
        return 0;
}

int run_fg(int argc, char **argv)
{
        if (argc < 2) {
                printf("Usage: fg pid, man fg!\n");
                return -1;
        }

        pid_t pid;
        int gid = -1;
        if (argv[1][0] == '%') {
                gid = atoi(argv[1] + 1);
                pid = get_pgid(gid);
                if (pid < 0) {
                        printf("fg %s: no such jobs\n", argv[1]);
                        return -1;
                }
        } else {
                pid = atoi(argv[1]);
        }
        if (kill( -(pid), SIGCONT ) < 0) {
                printf("fg %d: job not found\n", pid);
                return -1;
        }

        tcsetpgrp(0, pid);
        if (gid > 0) {
                 set_group_status(gid, CONTINUED);
                 __print_group_status(gid);
                 if (waiting_group(gid) >= 0)
                        remove_group(gid);
        } else {
                waiting_procs(pid);
        }
        signal(SIGTTOU, SIG_IGN);
        tcsetpgrp(0, getpid());
        signal(SIGTTOU, SIG_IGN);
        return 0;
}

int run_bg(int argc, char **argv)
{
        if (argc < 2) {
                printf("Usage: bg pid, man bg!!\n");
                return -1;
        }

        pid_t pid;
        int gid = -1;
        if (argv[1][0] == '%') {
                gid = atoi(argv[1] + 1);
                pid = get_pgid(gid);
                if (pid < 0) {
                        printf("bg %s: no such jobs\n", argv[1]);
                        return -1;
                }
        } else {
                pid = atoi(argv[1]);
        }
        if (kill( -(pid), SIGCONT ) < 0) {
                printf("bg %d: job not found\n", pid);
                return -1;
        }
        if (gid > 0) {
                set_group_status(gid, CONTINUED);
                __print_group_status(gid);
        }
        return 0;
}

int run_export(int argc, char **argv)
{
        if (argc < 2) {
                printf("Usage: export KEY=VALUE, man export!\n");
                return -1;
        }
        return putenv(argv[1]);
}

int run_unset(int argc, char **argv)
{
        if (argc < 2) {
                printf("Usage: unset KEY, man unset!\n");
                return -1;
        }
        return unsetenv(argv[1]);
}

int run_exit()
{
        exit(0);
}

int exec_builtin_cmds(Process* p)
{
        int status = 1;
        int p_type = p->type;
        if (p_type == EXIT)
                run_exit();
        else if (p_type == CD)
                run_cd(p->argc, p->argv);
        else if (p_type == FG)
                run_fg(p->argc, p->argv);
        else if (p_type == BG)
                run_bg(p->argc, p->argv);
        else if (p_type == EXPORT)
                run_export(p->argc, p->argv);
        else if (p_type == UNSET)
                run_unset(p->argc, p->argv);
        else if (p_type == JOBS)
                run_jobs(p->argc, p->argv);
        else
                status = EXTERNAL;

        return status;
}

int exec_procs(ProcessGroup *pg, Process *p, int fd_in, int fd_out, int mode)
{
        p->status = RUNNING;
        if (p->type != EXTERNAL && exec_builtin_cmds(p))
                return 0;

        int status = EXTERNAL;
        pid_t child_pid = fork();
        if (child_pid < 0) return -1;
        else if (child_pid == 0) {
                signal(SIGINT, SIG_DFL); signal(SIGQUIT, SIG_DFL); signal(SIGTSTP, SIG_DFL);
                signal(SIGTTIN, SIG_DFL); signal(SIGTTOU, SIG_DFL); signal(SIGCHLD, SIG_DFL);

                p->pid = getpid();
                if (pg->pgid > 0)
                        setpgid(0, pg->pgid);
                else {
                        pg->pgid = p->pid;
                        setpgid(0, pg->pgid);
                }

                // STDIN and STDOUT
                if (fd_in != 0) {
                        dup2(fd_in, 0); close(fd_in);
                }
                if (fd_out != 1) {
                        dup2(fd_out, 1); close(fd_out);
                }

                if (execvp(p->argv[0], p->argv) < 0) {
                        printf("\"%s\" command not found\n", p->argv[0]);
                        exit(0);
                }
                exit(0);
        }
        else {
                p->pid = child_pid;
                if (pg->pgid > 0)
                        setpgid(child_pid, pg->pgid);
                else {
                        pg->pgid = p->pid;
                        setpgid(child_pid, pg->pgid);
                }

                if (mode == FRONT) {
                        tcsetpgrp(0, pg->pgid);
                        status = waiting_group(pg->index);
                        signal(SIGTTOU, SIG_IGN);
                        tcsetpgrp(0, getpid());
                        signal(SIGTTOU, SIG_DFL);
                }
        }
        return status;
}

int exec_pgs(ProcessGroup *pg)
{
        int status = EXTERNAL;
        int fd_in = 0;
        int fd_out = 1;
        int fds[2];
        int gid = -1;

        Process *p;
        scan_zombie();

        if (pg->root_proc->type == EXTERNAL)
                gid = insert_group(pg);

        for (p = pg->root_proc; p != NULL; p = p->next_proc)
        {
                if ((p == pg->root_proc) && p->path_in != NULL) {
                        fd_in = open(p->path_in, O_RDONLY);
                        if (fd_in < 0) {
                                printf("No such file or directory: %s\n", p->path_in);
                                remove_group(gid);
                                return -1;
                        }
                }
                if (p->next_proc != NULL) {
                        pipe(fds);
                        status = exec_procs(pg, p, fd_in, fds[1], PIPE);
                        close(fds[1]);
                        fd_in = fds[0];
                } else {
                        fd_out = 1;
                        if (p->path_out != NULL) {
                                fd_out = open(p->path_out, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
                                if (fd_out < 0) fd_out = 1;
                        }
                        status = exec_procs(pg, p, fd_in, fd_out, pg->mode);
                }
        }
        if (pg->root_proc->type == EXTERNAL) {
                if (status >= 0 && pg->mode == FRONT)
                        remove_group(gid);
                else if (pg->mode == BACK)
                        __print_group_status(gid);
        }
        return status;
}

