#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>

#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NR_GROUPS 15
#define MAX_PATH 1024
#define MAX_CMDS 1024
#define MAX_TOKENS 100

enum EXEC       { BACK = 0, FRONT = 1, PIPE = 2 };
enum CMDS       { EXTERNAL = 0, EXIT = 1, CD = 2, FG = 3, BG = 4, EXPORT = 5, UNSET = 6, JOBS = 7 };
enum STATUS     { RUNNING = 0, DONE = 1, SUSPENDED = 2, CONTINUED = 3, TERMINATED = 4 };

typedef struct Process {

        struct Process *next_proc;

        pid_t           pid;
        char            *cmds;
        char            **argv;
        char            *path_in;
        char            *path_out;
        int             argc;

        int             status;
        int             type;
} Process;

/* Process Group */
typedef struct ProcessGroup {

        struct Process *root_proc;
        char            *commands;
        int             index;
        int             mode;
        pid_t           pgid;
} ProcessGroup;

typedef struct Shell {

        char            curr_user[MAX_TOKENS];
        char            pwd_dir[MAX_PATH];
        char            curr_dir[MAX_PATH];

        struct ProcessGroup *pgs[NR_GROUPS + 1];
} Shell;

Shell *curr_shell;

#endif
