#include "groups.h"
#include "print.h"

ProcessGroup *get_group(int index)
{
        return (index > NR_GROUPS) ? (NULL) : curr_shell->pgs[index];
}

int get_pgid(int index)
{
        ProcessGroup *pg = get_group(index);
        return (pg == NULL) ? -1 : pg->pgid;
}

int get_procs_in_which_group(pid_t pid)
{
        for (int i = 1; i <= NR_GROUPS; ++i)
        {
                if (curr_shell->pgs[i] != NULL) {
                        for (Process *p = curr_shell->pgs[i]->root_proc;
                                        p != NULL; p = p->next_proc)
                        {
                                if (p->pid == pid)
                                        return i;
                        }
                }
        }
        return -1;
}

int get_empty_group_index()
{
        for (int i = 1; i <= NR_GROUPS; ++i)
        {
                if (curr_shell->pgs[i] == NULL)
                        return i;
        }
        return -1;
}

int get_total_procs(int index, int proc_status)
{
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return -1;

        int ret = 0;
        for (Process *p = curr_shell->pgs[index]->root_proc;
                        p != NULL; p = p->next_proc)
        {
                // this process still running.
                if (proc_status == 2) ret++;
        }
        return ret;
}

int free_group(int index)
{
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return -1;

        Process *p = curr_shell->pgs[index]->root_proc;
        Process *ptr;
        // delete the hole group in pgs.
        while (p != NULL)
        {
                ptr = p->next_proc;
                free(p->cmds); free(p->argv);
                free(p->path_in); free(p->path_out);
                free(p);
                p = ptr;
        }
        free(curr_shell->pgs[index]->commands);
        free(curr_shell->pgs[index]);
        return 0;
}

int insert_group(ProcessGroup *g)
{
        int index = get_empty_group_index();
        if (index < 0) return -1;

        // new group insert into pgs.
        g->index = index;
        curr_shell->pgs[index] = g;
        return index;
}

int remove_group(int index)
{
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return -1;

        free_group(index);
        curr_shell->pgs[index] = NULL;
        return 0;
}

int check_all_procs_in_group_done(int index)
{
        // group doesn't exist.
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return 0;

        // check all process in group is done status.
        for (Process *p = curr_shell->pgs[index]->root_proc;
                        p != NULL; p = p->next_proc)
        {
                if (p->status != DONE)
                        return 0;
        }
        return 1;
}

int set_procs_status(pid_t pid, int status)
{
        Process *p;
        for (int i = 1; i <= NR_GROUPS; ++i)
        {
                if (curr_shell->pgs[i] == NULL) continue;

                for (p = curr_shell->pgs[i]->root_proc;
                                p != NULL; p = p->next_proc)
                {
                        if (p->pid == pid) {
                                p->status = status;
                                return 0;
                        }
                }
        }
        return -1;
}

int set_group_status(int index, int status)
{
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return -1;
        // update hole group process status.
        for (Process *p = curr_shell->pgs[index]->root_proc;
                        p != NULL; p = p->next_proc)
        {
                if (p->status != DONE)
                        p->status = status;
        }
        return 0;
}

int waiting_procs(pid_t pid)
{
        int status = 0;
        waitpid(pid, &status, WUNTRACED);

        if (WIFEXITED(status))
                set_procs_status(pid, DONE);
        else if (WIFSIGNALED(status))
                set_procs_status(pid, TERMINATED);
        else if (WSTOPSIG(status)) {
                status = -1;
                set_procs_status(pid, SUSPENDED);
        }
        return status;
}

int waiting_group(int index)
{
        if (curr_shell->pgs[index] == NULL || index > NR_GROUPS)
                return -1;

        int p_count = get_total_procs(index, 2);
        int wait_p_count = 0;
        int wait_pid = -1;

        int status = 0;
        for (; wait_p_count < p_count;)
        {
                // set WIFSTOPPED bit.
                wait_p_count++;
                wait_pid = waitpid(-(curr_shell->pgs[index]->pgid), &status, WUNTRACED);

                if (WIFEXITED(status))
                        set_procs_status(wait_pid, DONE);
                else if (WIFSIGNALED(status))
                        set_procs_status(wait_pid, TERMINATED);
                else if (WSTOPSIG(status)) {
                        status = -1;
                        set_procs_status(wait_pid, SUSPENDED);
                        if (wait_p_count == p_count)
                                __print_group_status(index);
                }
        }
        return status;
}
