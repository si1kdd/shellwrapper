#include "signal.h"
#include "groups.h"
#include "print.h"

#define UNUSED(x) (void)(x)

void scan_zombie()
{
        pid_t pid;
        int status;

        // Here you can use wait4(), wait3(), or waitpid() APIs.
        // Different implementation with different Platforms (MinGW, BSD, Linux etc).
        while ((pid = waitpid(-1, &status, WNOHANG | WUNTRACED | WCONTINUED)) > 0)
        {
                if (WIFEXITED(status))
                        set_procs_status(pid, DONE);
                else if (WIFSTOPPED(status))
                        set_procs_status(pid, SUSPENDED);
                else if (WIFCONTINUED(status))
                        set_procs_status(pid, CONTINUED);

                int index = get_procs_in_which_group(pid);
                int done_flag = check_all_procs_in_group_done(index);
                if (done_flag && index > 0) {
                        __print_group_status(index);
                        remove_group(index);
                }
        }
}

void sigint_handler(int s)
{
        UNUSED(s);
        printf("\n");
}
