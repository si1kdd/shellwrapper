#ifndef __PARSE_H__
#define __PARSE_H__

#include "utils.h"

char *read_line();
int get_cmds_type(char *cmds);

Process* parse_tokens(char *slice);

ProcessGroup *parse_cmds(char *line);

#endif
