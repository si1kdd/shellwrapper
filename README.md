# Simple Shell wrapper

* A little shell wrapper, a implementation by using Unix user level API.
* Both work on Linux and BSD.
    * (Untested on MacOS !!)

## Build:

- make; ./shell_wrapper

* * *

## Features:
- Supporting built-in commands:
        - cd, exit, export, unset, jobs, fg, bg.

- Backgroud process (&)
- Ctrl-C and Ctrl-Z signal handling.
- Job control commands: jobs, fg, bg, Ctrl-Z key press.
- I/O redirection.
- Process Pipeline.
- Filename expansion with operator * and ?
- Support EOF (Ctrl-D) exit, but you should always use the "exit" command.

- !Please don't use some special char like : "", @, $...
        - The shell parsing function is not perfect, should have fix it.

* * *

## Example Input:


- Job Control:
```
        sleep 50 &
        sleep 60 &
        jobs
        fg %1
        bg %2
        jobs
        fg %2
        # Ctrl-Z
        fg %1
        # Ctrl-C
        jobs
```
- Pipe , I/O redirection , Expansion, Directroy changing:
```
        ps -o pid,sid,pgid,ppid | cat | cat | tr A-Z a-z
        cat < ./*.c | tee output
        ls s?ell.? &
        cd /
```
