#ifndef __GROUPS_H_
#define __GROUPS_H_

#include "utils.h"

ProcessGroup *get_group(int index);
int get_pgid(int index);

int get_procs_in_which_group(pid_t pid);
int get_empty_group_index();

int get_total_procs(int index, int proc_status);

int free_group(int index);
int insert_group(ProcessGroup *g);
int remove_group(int index);

int check_all_procs_in_group_done(int index);

int set_procs_status(pid_t pid, int status);
int set_group_status(int index, int status);

int waiting_procs(pid_t pid);
int waiting_group(int index);

#endif
